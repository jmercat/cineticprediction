#include "nonlinearmodel.h"


template <int stateSize>
void NonLinearStationaryModel<stateSize>::predictInPlace(Estimation &estimation)
{
    assert(stateSize == estimation.state.size());
    this->computeFunctionAndJacobian(estimation.state);

    // now that the function and its Jacobian are defined
    // we use it to make the prediction :
    estimation.state = mF*estimation.state;
    estimation.covariance = mJacobian*estimation.covariance*mJacobian.transpose() +mQ;
}


CTWNA::CTWNA(const float &dt, const float &maximumAccel, const float &maximumYawAccel)
    :mDt(dt)
{
    //set the process noise matrix
    mQ.setZero();
    // x and vx noise block
    mQ.block<2,2>(0,0) << mDt*mDt*mDt/3, mDt*mDt/2,
                            mDt*mDt/2 ,    mDt  ;
    // y and vy noise block(same model)
    mQ.block<2,2>(2,2) = mQ.block<2,2>(0,0);
    mQ *= maximumAccel*maximumAccel*mDt;

    // yaw rate noise
    mQ(4,4) = mDt*maximumYawAccel*maximumYawAccel*mDt;

    //set the constant parameters of the function matrix
    mF.setZero();
    mF(0,0) = 1;
    mF(2,2) = 1;
    mF(4,4) = 1;

}

void CTWNA::computeFunctionAndJacobian(const Eigen::VectorXf &state)
{
    //given state has the correct size
    assert(5 == state.size());
    float w = state(4);

    float vx = state(1);
    float vy = state(3);

    // If the yaw rate (state(4) stored here in w) is close to 0,
    // we use the first order approximation of the function and jacobian
    if(fabs(w)>mDt*mDt)
    {
        //The function is defined with the matrix mF depending of
        float cwmDt = cos(w*mDt);
        float swmDt = sin(w*mDt);
        mF.col(1) << swmDt/w,
                     cwmDt,
                     (1.-cwmDt)/w,
                     swmDt,
                     0;
        mF.col(3) << -(1.-cwmDt)/w,
                     -swmDt,
                     swmDt/w,
                     cwmDt,
                     0;

        mJacobian = mF;

        mJacobian(0,4) = cwmDt*mDt*vx/w -  swmDt   *vx/(w*w)
                        -swmDt*mDt*vy/w - (cwmDt-1)*vy/(w*w);

        mJacobian(2,4) = swmDt*mDt*vx/w - (1-cwmDt)*vx/(w*w)
                        +cwmDt*mDt*vy/w -    swmDt *vy/(w*w);

        mJacobian(1,4) = -(swmDt*mDt*vx-cwmDt*mDt*vy);
        mJacobian(3,4) =   cwmDt*mDt*vx-swmDt*mDt*vy;

    }else
    {
        mF.col(1) << mDt,
                      1,
                      0,
                      0,
                      0;

        mF.col(3) <<  0,
                      0,
                     mDt,
                      1,
                      0;
        mJacobian = mF;

        mJacobian.col(4) << -mDt*mDt*vy/2,
                               -mDt*vy  ,
                             mDt*mDt*vx/2,
                               mDt*vx   ,
                                 1     ;
    }

}
