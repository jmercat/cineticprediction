#ifndef LINEARMODEL_H
#define LINEARMODEL_H

#include "model.h"


/**
 *  linearStationaryDecoupledModel is a generic linear model with constant model matrix
 *  and command and constant process covariance. It uses a state with \a size parameters for both coordinates (x and y).
 *  It can be implemented by defining the state size, the model matrix, the command vector and the process covariance.
 *
 */
template <int stateSize1D>
class LinearStationaryDecoupledModel: Model
{
public:
    void predictInPlace(Estimation &estimation) override;

protected:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    //model matrix
    Eigen::Matrix<float,stateSize1D,stateSize1D> mA;
    //command matrix times command vector
    Eigen::Matrix<float,stateSize1D,1> mBU;
    //process covariance
    Eigen::Matrix<float,stateSize1D,stateSize1D> mQ;

private:
    Estimation extractCoord(const Estimation &estimation, bool isX) const;
    Estimation mergeCoord(const Estimation &x, const Estimation &y) const;
    void predictInPlace1D(Estimation &estimation) const;
};

/**
 * @brief The CWNA class implements the CWNA model that is continuous white noise acceleration.
 * it is a nearly constant speed model that treats x and y coordinates separatedly.
 * Thus it is a linearStationaryDecoupledModel of 2 state parameter for each coordinate.
 * It uses a 4 parameters state (x, vx, y, vy)'
 */
class CWNA: public LinearStationaryDecoupledModel<2>
{
public:
    /**
     * @brief CWNA model implementation
     * @param dt time constant
     * @param maximumAccel approximate maximum accel
     *                     indication: dt*maximumAccel should be kept very small compared to state speed (sqrt(vx*vx+vy*vy))
     */
    CWNA(const float dt, const float maximumAccel);
};

/**
 * @brief The CWPA class implements the CWPA model that is continuous weiner process acceleration.
 * It is a nearly constant acceleration model that treats x and y coordinates separatedly.
 * Thus it is a linearStationaryDecoupledModel of 3 state parameters for each coordinate.
 * So it uses a 6 parameters state (x, vx, ax, y, vy, ay)'
 */
class CWPA: public LinearStationaryDecoupledModel<3>
{
public:
    /**
     * @brief CWPA model implementation
     * @param dt time constant
     * @param maximumAccel approximate maximum accel variation
     *                     indication: should keep maximumAccel very small compared to state accel (=sqrt(ax*ax+ay*ay))
     */
    CWPA(const float dt, const float maximumAccel);
};


/**
 * @brief The EAA class implements the EAA model, that is exponentially autocorrelated acceleration.
 * It is a non-constant acceleration model using E[accel(t)*accel(t+tau)] = s*s*exp(-a*tau).
 * It treats x and y coordinates separatedly.
 * Thus it is a linearStationaryDecoupledModel of 3 state parameters for each coordinate.
 * So it uses a 6 parameters state (x, vx, ax, y, vy, ay)'
 */
class EAA: public LinearStationaryDecoupledModel<3>
{
public:
    /**
     * @brief EAA model imlementation
     * @param dt time step
     * @param maximumAccel approximate maximum accel variation
     * @param timeConstant time constant of the accel correlation, decorrelation time is 2*timeConstant
     *                     indication : use values in [20, 1]
     *                                  total prediction time divided by time constant shoul be very small
     * @param probabilityExtreme probability that the acceleration is +/- \a maximumAccel
     *                           indication : use value arround 1./8
     * @param probabilityNull probability tha the acceleration is null
     *                        indication : use value arround 1./2
     */
    EAA(const float dt, const float maximumAccel, const float timeConstant,
        const float probabilityExtreme, const float probabilityNull);
};


/**
 * @brief The DWNA class implements DWNA model, that is discret white noise acceleration.
 * It is a nearly constant speed model that treats x and y coordinates separatedly.
 * Thus it is a linearStationaryDecoupledModel of 2 state parameter for each coordinate.
 * It uses a 4 parameters state (x, vx, y, vy)'
 */
class DWNA : public LinearStationaryDecoupledModel<2>
{
public:
    /**
     * @brief DWNA model implementation
     * @param dt time step
     * @param maximumAccel maximum acceleration (or acceleration amplitude)
     *                     indication: maximumAccel*dt should be kept very small compared to state velocity (sqrt(vx*vx+vy*vy))
     */
    DWNA(const float dt, const float maximumAccel);
};


class DWPA: public LinearStationaryDecoupledModel<3>
{
public:
    /**
     * @brief DWPA model implementation
     * @param dt time step
     * @param maximumAccel maximum acceleration variation (maximum acceleration increment in a time step)
     *                     indication: maximumAccel should be kept very small compared to state acceleration (sqrt(ax*ax+ay*ay))
     */
    DWPA(const float dt, const float maximumAccel);
};
#endif // LINEARMODEL_H
