#ifndef EstimationFileOutput_H
#define EstimationFileOutput_H

#include <iostream>
#include <fstream>

#include "estimation.h"

class EstimationFileOutput
{
public:
    EstimationFileOutput(std::string fileName,int which = 0);
    ~EstimationFileOutput();

    void write(Estimation estimation);
private:
    std::ofstream output;
    int mWhich;
};

#endif // EstimationFileOutput_H
