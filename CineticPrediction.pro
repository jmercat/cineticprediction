TEMPLATE = app
CONFIG += console c++11
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
#    predictor.cpp \
    linearmodel.cpp \
    estimationfileoutput.cpp \
    nonlinearmodel.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
#    predictor.h \
    estimation.h \
    model.h \
    linearmodel.h \
    estimationfileoutput.h \
    nonlinearmodel.h

DISTFILES += plotResults.gnp

OTHER_FILES += \
    plotResult.gnp


