#include "linearmodel.h"

using namespace Eigen;

template<int stateSize1D>
void LinearStationaryDecoupledModel<stateSize1D>::predictInPlace(Estimation &estimation)
{
    //impose 2D
    assert(2*stateSize1D == estimation.state.size());

    //make the estimation for each coordinate (2D)
    Estimation estimationX = this->extractCoord(estimation,0);
    Estimation estimationY = this->extractCoord(estimation,1);
    this->predictInPlace1D(estimationX);
    this->predictInPlace1D(estimationY);

    //set output estimation with the new values
    estimation = mergeCoord(estimationX,estimationY);
}

template<int stateSize1D>
void LinearStationaryDecoupledModel<stateSize1D>::predictInPlace1D(Estimation &estimation) const
{
    estimation.state = mA*estimation.state+mBU;
    estimation.covariance = mA*estimation.covariance*mA.transpose() + mQ;
}

// warnwarn: these two function below extractCoord and mergeCoord are called at every advance call and should be optimized ...
// this is not optimal at all yet. Maybe estimation should be a class that avoids this extract-merge use.

template<int stateSize1D>
Estimation LinearStationaryDecoupledModel<stateSize1D>::extractCoord(const Estimation& estimation, bool isY) const
{
    if(!isY)
    {
        VectorXf stateCoorX = estimation.state.head<stateSize1D>();
        MatrixXf covarianceX = estimation.covariance.block<stateSize1D,stateSize1D>(0,0);
        Estimation estimationX;
        estimationX.state = stateCoorX;
        estimationX.covariance = covarianceX;
        return estimationX;
    }else
    {
        VectorXf stateCoorY = estimation.state.tail<stateSize1D>();
        MatrixXf covarianceY = estimation.covariance.block<stateSize1D,stateSize1D>(stateSize1D,stateSize1D);
        Estimation estimationY;
        estimationY.state = stateCoorY;
        estimationY.covariance = covarianceY;
        return estimationY;
    }
}

template<int stateSize1D>
Estimation LinearStationaryDecoupledModel<stateSize1D>::mergeCoord(const Estimation& x, const Estimation& y) const
{
    Estimation merged = {.state = VectorXf(stateSize1D*2), .covariance = MatrixXf(stateSize1D*2,stateSize1D*2)};

    merged.state.head<stateSize1D>() = x.state;
    merged.covariance.block<stateSize1D,stateSize1D>(0,0) = x.covariance;
    merged.state.tail<stateSize1D>() = y.state;
    merged.covariance.block<stateSize1D,stateSize1D>(stateSize1D,stateSize1D) = y.covariance;

    return merged;
}

//Below are implementations of different models

CWNA::CWNA(const float dt, const float maximumAccel)
{
    mA << 1, dt,
          0,  1;

    mQ << dt*dt*dt/3, dt*dt/2,
            dt*dt/2 ,    dt;
    mQ *= maximumAccel*maximumAccel*dt;

    mBU.setZero();
}


CWPA::CWPA(const float dt, const float maximumAccel)
{
    mA << 1, dt, dt*dt/2,
          0,  1,   dt,
          0,  0,   1;

    mQ << dt*dt*dt*dt*dt/20, dt*dt*dt*dt/8, dt*dt*dt/6,
            dt*dt*dt*dt/8  ,   dt*dt*dt/3 ,  dt*dt/2  ,
              dt*dt*dt/6   ,     dt*dt/2  ,    dt;
    mQ *= maximumAccel*maximumAccel/dt;

    mBU.setZero();
}


EAA::EAA(const float dt, const float maximumAccel, const float timeConstant,
         const float probabilityExtreme, const float probabilityNull)
{
    float a = 1./timeConstant;
    float s = maximumAccel*maximumAccel/3*(1+4*probabilityExtreme-probabilityNull);
    float eadt = exp(-a*dt);

    mA << 1 , dt , (a*dt-1+eadt)/(a*a) ,
          0 ,  1 ,   ( 1 - eadt)/a     ,
          0 ,  0 ,       eadt          ;

    mQ << dt*dt*dt*dt*dt/20, dt*dt*dt*dt/8, dt*dt*dt/6,
            dt*dt*dt*dt/8  ,   dt*dt*dt/3 ,  dt*dt/2  ,
              dt*dt*dt/6   ,     dt*dt/2  ,    dt;
    mQ *= 2*a*s*s;

    mBU.setZero();
}



DWNA::DWNA(const float dt, const float maximumAccel)
{
    mA << 1, dt,
          0,  1;

    mQ << dt*dt*dt*dt/4 , dt*dt*dt/3,
            dt*dt*dt/3  ,    dt     ;
    mQ *= maximumAccel*maximumAccel;

    mBU.setZero();
}


DWPA::DWPA(const float dt, const float maximumAccel)
{
    mA << 1 , dt , dt*dt/2,
          0 ,  1 ,   dt   ,
          0 ,  0 ,    1   ;
    mQ << dt*dt*dt*dt/4 , dt*dt*dt/3 , dt*dt/2 ,
           dt*dt*dt/2   ,    dt*dt   ,    dt   ,
            dt*dt/2     ,     dt     ,     1   ;
    mQ *= maximumAccel*maximumAccel;

    mBU.setZero();
}
