#include "estimationfileoutput.h"

using namespace Eigen;


template<typename T>
T angle(T x, T y,bool isDegree=false)
{
    float pi = 3.141592653589793238;
    float angle;

    if(x>0 && y>=0) //first quadrant
        angle = atan(y/x);
    else if(x<0 && y>=0) //second quadrant
        angle = pi-atan(y/(-x));
    else if(x<0 && y<=0) //third quadrant
        angle = 3*pi/2-atan(y/x);
    else if(x>0 && y <=0) //fourth quadrant
        angle = 2*pi+atan(y/x);
    else
        return 0;

    angle = fmod(angle,pi);
    if(isDegree)
        angle *= 180./pi;
    return angle;
}

EstimationFileOutput::EstimationFileOutput(std::string fileName,int which):
    mWhich(which)
{
    output.open(fileName);
}



EstimationFileOutput::~EstimationFileOutput()
{
    output.close();
}

void EstimationFileOutput::write(Estimation estimation)
{
    int stride = estimation.state.size()/2;
    MatrixXf covariance(2,2);
    covariance << estimation.covariance(mWhich,mWhich), 0,
                            0,                        estimation.covariance(mWhich+stride,mWhich+stride);
    EigenSolver<MatrixXf> es(covariance);
    if(es.info()!= Success)
    {
        std::cout << es.info() << std::endl;
    }
    VectorXcf eVal = es.eigenvalues();
    MatrixXcf eVec = es.eigenvectors();
    float s = 5.991; //for a 95% interval

    //writing x, y, major diameter, minor diameter, angle in degrees
    output << estimation.state(mWhich) << ", " << estimation.state(mWhich+stride) << ", "
                  << 2*sqrt(s*eVal(0).real()) << ", "
                  << 2*sqrt(s*eVal(1).real()) << ", "
                  << angle(eVec(0,0).real(),eVec(0,1).real(),true)
                  << std::endl;
}

