#ifndef ESTIMATION_H
#define ESTIMATION_H

#include <memory>
#include "eigen3/Eigen/Dense"

//warnwarn Estimation
/**
 * @brief The Estimation struct defines an estimation as a state and its covariance matrix
 * @todo  should be made into a class:\
 *          Need for templated size of state and covariance matrix.\
 *          A lot of usefull access function could be added.\
 *              ex: getting the x,y part of covariance matrix, or x,vx or y,vy\
 *          Need to make sure that the ordering is done properly by the class and not the function\
 *          Could have a inherited class that alows decoupled states to be transparently stored in\
 *          two different vectors and covariance matrix (avoid storing 0, reduce complexity)\
 */
struct Estimation{Eigen::VectorXf state; Eigen::MatrixXf covariance;};


#endif // ESTIMATION_H
