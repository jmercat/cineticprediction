#ifndef MODEL_H
#define MODEL_H

#include "estimation.h"

class Model
{
public:
    virtual void predictInPlace(Estimation& estimation) = 0;
};

#endif // MODEL_H
