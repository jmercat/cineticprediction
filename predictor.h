#ifndef PREDICTOR_H
#define PREDICTOR_H

#include <vector>

#include "eigen3/Eigen/Dense"
#include "estimation.h"

class predictor
{
public:
    predictor(Eigen::MatrixXf model, Eigen::MatrixXf command, Eigen::MatrixXf processNoise);

    void advance(Estimation& estimation, const Eigen::VectorXf& commandVector);
    Estimation predict(Estimation estimation, const Eigen::VectorXf &commandVector);
    std::vector<Estimation> predict(Estimation estimation, const std::vector<Eigen::VectorXf> &commands);

    //setters and accessors

    Eigen::MatrixXf model() const;
    void setModel(const Eigen::MatrixXf &model);

    Eigen::MatrixXf command() const;
    void setCommand(const Eigen::MatrixXf &command);

    Estimation estimation() const;
    void setEstimation(const Estimation &estimation);

    Eigen::MatrixXf processNoise() const;
    void setProcessNoise(const Eigen::MatrixXf &processNoise);

private:

};

#endif // PREDICTOR_H
