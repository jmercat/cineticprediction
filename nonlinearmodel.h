#ifndef NONLINEARMODEL_H
#define NONLINEARMODEL_H

#include "model.h"

/**
 * @brief The NonLinearStationaryModel class describes a generic prediction
 * model as a non linear model written as X(t+1) = F(X)*X and P(t+1) = J(X)*P(t)*J(X)'+Q
 * With F a matrix depending on the state X and J its Jacobian.
 * It may be implemented by defining the function computeFunctionAndJacobian that should
 * updates values of mF and mJacobian using values from the current state.
 */
template <int stateSize>
class NonLinearStationaryModel: public Model
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    /**
     * @brief predictInPlace compute the prediction of the next step for the given estimation
     * @param estimation state and covariance to be updated by prediction
     */
    void predictInPlace(Estimation &estimation) override;
protected:
    /**
     * @brief computeFunctionAndJacobian compute the function matrix and its Jacobian for current use
     * @param state current state
     */
    virtual void computeFunctionAndJacobian(const Eigen::VectorXf &state) = 0;

    //model function matrix
    Eigen::Matrix<float,stateSize,stateSize> mF;
    //jacobian of the model function
    Eigen::Matrix<float,stateSize,stateSize> mJacobian;
    //process covariance
    Eigen::Matrix<float,stateSize,stateSize> mQ;
};

/**
 * @brief The CTWNA class implements the CT(WNA) model,
 * that is coordinated turn model (white noise acceleration)
 */
class CTWNA : public NonLinearStationaryModel<5>
{
public:
    /**
     * @brief CTWNA
     * @param dt time step
     * @param maximumAccel approximate maximum accel
     *                     indication: dt*maximumAccel should be kept very small compared to state speed (sqrt(vx*vx+vy*vy))
     * @param maximumYawAccel approximate maximum yaw rate
     *                        indication: is equal to turn angle/time of turn
     */
    CTWNA(const float& dt, const float& maximumAccel, const float& maximumYawAccel);
private:
    /**
     * @brief computeFunctionAndJacobian
     * @param state current state
     */
    void computeFunctionAndJacobian(const Eigen::VectorXf &state) override;
    // time step
    const float mDt;
};

#endif // NONLINEARMODEL_H
