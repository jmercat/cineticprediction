#include "predictor.h"

using namespace Eigen;

predictor::predictor(MatrixXf model, MatrixXf commandMatrix, MatrixXf processNoise):
mModel(model), mCommand(commandMatrix), mProcessNoise(processNoise)
{
}

void predictor::advance(Estimation &estimation, const VectorXf& commandVector)
{
    estimation.state = mModel*estimation.state + mCommand*commandVector;
    estimation.covariance = mModel*estimation.covariance*mModel.transpose() + mProcessNoise;
}

Estimation predictor::predict(Estimation estimation, const VectorXf& commandVector)
{
    this->advance(estimation, commandVector);
    return estimation;
}

std::vector<Estimation> predictor::predict(Estimation estimation, const std::vector<VectorXf> &commands)
{
    std::vector<Estimation> predictions;
    Estimation nextStep = estimation;
    for(const VectorXf& command : commands)
    {
        this->advance(nextStep,command);
        predictions.push_back(nextStep);
    }
}

MatrixXf predictor::model() const
{
    return mModel;
}

void predictor::setModel(const MatrixXf &model)
{
    mModel = model;
}
MatrixXf predictor::command() const
{
    return mCommand;
}

void predictor::setCommand(const MatrixXf &command)
{
    mCommand = command;
}
Eigen::MatrixXf predictor::processNoise() const
{
    return mProcessNoise;
}

void predictor::setProcessNoise(const Eigen::MatrixXf &processNoise)
{
    mProcessNoise = processNoise;
}




