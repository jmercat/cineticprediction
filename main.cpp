#include <iostream>
#include "estimationfileoutput.h"
#include "linearmodel.h"
#include "nonlinearmodel.h"

using namespace std;
using namespace Eigen;

int main()
{
    float dt = 0.1;
    float maxAccel = 0.02;
    float maxAccelVar = 0.1;
    float maxYawRate = (3./2)/5; //angle of turn / time of turn
    float Ttot = 5;
    CWNA cwna(dt, maxAccel);
    CWPA cwpa(dt,maxAccelVar);
    EAA eaa(dt, maxAccel,20,1./4,1./2);
    DWPA dwpa(dt, maxAccelVar);
    CTWNA ctwna(dt,maxAccel,maxYawRate);

    Estimation estimation3 = {.state = VectorXf(6), .covariance = MatrixXf(6,6)};
    Estimation estimation2 = {.state = VectorXf(4), .covariance = MatrixXf(4,4)};
    Estimation estimation5 = {.state = VectorXf(5), .covariance = MatrixXf(5,5)};

    //state variables
    float x,vx, ax, y, vy, ay, w;
    x = 0;
    vx = 10;
    ax = 0;
    y = 0;
    vy = 20;
    ay = -10;
    w = 0.1;

    // covariances
    float sxx, syy;
    sxx = 0.7;
    syy = 0.5;

    estimation3.state << x, vx, ax, y, vy, ay;

    estimation2.state << x, vx, y, vy;

    estimation5.state << x, vx, y, vy, w;

    estimation3.covariance.setZero();
    estimation3.covariance(0,0) = sxx;
    estimation3.covariance(3,3) = syy;

    estimation2.covariance.setZero();
    estimation2.covariance(0,0) = sxx;
    estimation2.covariance(2,2) = syy;

    estimation5.covariance.setZero();
    estimation5.covariance(0,0) = sxx;
    estimation5.covariance(2,2) = syy;

    EstimationFileOutput writer("result.csv");
    for(int i = 0; i< Ttot/dt; i++)
    {
        ctwna.predictInPlace(estimation5);
        if(i%int(1./dt)==0)
            writer.write(estimation5);
    }
    return 0;
}

